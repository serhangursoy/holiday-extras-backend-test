/*
 Why LowDB? Because I don't want to waste resource with MongoDB. At least for this
 super simple project, it's really unnecessary to have a MongoDB. Also, I'm not
 going to encrypt the db file. To do that, all you need to do is adding this line to
    { serialize: (data) => encrypt(JSON.stringify(data)), deserialize: (data) => JSON.parse(decrypt(data)) }
 FileSync file as second parameter.
*/
const low = require('lowdb')
const FileSync = require('lowdb/adapters/FileSync')

// Unlike predictable numerical id's, we will use alphanumerical id's with simple hash
// so that session hijacking, or any sorta injection will be more unlikely.
const shortid = require('shortid')
const template = require("../templates/userdb");

const adapter = new FileSync('database/users/db.json')
const db = low(adapter)

// Set some defaults (required if your JSON file is empty)
db.defaults(template.userTemplate).write()

exports.getUsers = () => {
  try {
    const users = db.get('users').value();
    return users;
  } catch (err) {
    console.error("Error! At Get Users ", err);
    return false;
  }
}

exports.getUser = (id) => {
  try {
    const user = db.get('users').find({
      id: id
    }).value();
    return user;
  } catch (err) {
    console.error("Error! At Get User with id " + id + " ", err);
    return false;
  }
}

exports.createUser = (data) => {
  try {
    const timestamp = new Date().getTime();
    const createdID = shortid.generate();
    db.get('users').push({
      id: createdID,
      email: data.email,
      givenName: data.givenName,
      familyName: data.familyName,
      created: timestamp,
      active: true
    }).write().id
    db.update('count', n => n + 1).write()
    return createdID;
  } catch (err) {
    console.log("Error! At Create User", err);
    return false;
  }
}

exports.deleteUser = (id) => {
  try {
    // We can do this in two ways. We can either deactive them, which is the secure way, or
    // we can really delete them. Which isn't that secure because it may cause problems in the future.
    // Secure way
    // db.get('users').find({ id: id }).assign({ active: false}).write();
    // Non secure way
    const res = db.get('users').remove({
      id: id
    }).write()
    if (res.length !== 0) {
      db.update('count', n => n - 1).write()
      return true;
    }
    return false;
  } catch (err) {
    console.error("Error! At Create User", err);
    return false;
  }
}

exports.updateUser = (id, data) => {
  try {
    const user = db.get('users').find({
      id: id
    })
    if (user) {
      user.assign(data).write();
      return true;
    } else {
      return false;
    }
  } catch (err) {
    console.error("Error! At updating user with id " + id + " ", err);
    return false;
  }
}
