userController = require("../controllers/userController");

var appRouter = function(app) {
  /* CREATE */
  app.post("/users/create", userController.createUser);
  /**** These two are not required actually. And in a way, they are against to usage of CRUD however many people chosing the use post for these requests ***/
  app.post("/users/delete/:id", userController.deleteUser);
  app.post("/users/:id", userController.updateUser);

  /* GET */
  app.get("/users", userController.getAllUsers);
  app.get("/users/:id", userController.getSingleUser);
  /* DELETE */
  app.delete("/users/delete/:id", userController.deleteUser);
  /* UPDATE */
  app.put("/users/:id", userController.updateUser);

  //// Handlers ////
  app.get("/", function(req, res) {
    res.status(200).send("Yaay! We are alive. If seeing this page wasnt your goal, then please try to check API Documentation");
  });
  app.get('*', function(req, res) {
    res.status(404).send("Not found. Do you know why? Because we dont. Please check documentation");
  });

}

module.exports = appRouter;
