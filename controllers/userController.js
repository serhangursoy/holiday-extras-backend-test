const userModel = require("../models/userModel");

exports.getAllUsers = (req, res, next) => {
  try {
    console.log("Get all users request");
    const users = userModel.getUsers();
    res.json({
      users
    });
  } catch (err) {
    res.status(500).send({
      status: "error",
      message: "Internal problems"
    });
  }
}

exports.getSingleUser = (req, res, next) => {
  try {
    console.log("Get single user request");
    const id = req.params.id;
    if (!id) {
      res.status(400).send({
        status: "error",
        message: "You need to specify an ID"
      });
      return;
    }
    const user = userModel.getUser(id);
    res.json({
      user
    });
  } catch (err) {
    console.log("Get single user request > FAIL");
    res.status(500).send({
      status: "error",
      message: "Internal problems"
    });
  }
}

exports.deleteUser = (req, res, next) => {
  try {
    const id = req.params.id;
    console.log("Delete user request");
    if (!id) {
      res.status(400).send({
        status: "error",
        message: "You need to specify an ID"
      });
      return;
    }
    const resp = userModel.deleteUser(id);
    if (resp) {
      res.status(200).send({
        status: "success",
        message: "Deleted user"
      });
    } else {
      console.log("Delete user request > FAIL");
      res.status(400).send({
        status: "error",
        message: "User deletion failed. Check if you have that user already"
      });
    }
  } catch (err) {
    console.log("Delete user request > FAIL");
    res.status(500).send({
      status: "error",
      message: "Internal problems"
    });
  }
}

exports.createUser = (req, res, next) => {
  const userReq = req.body;
  console.log("Create user request");
  if (userReq.email && userReq.givenName && userReq.familyName && checkDataValidity(userReq)) {
    const createdUser = userModel.createUser(userReq);
    if (!createdUser) {
      console.log("Create user request > FAIL");
      res.status(500).send({
        status: "error",
        message: "Internal problems"
      });
      return;
    }
    console.log("Create user request > SUCCESS");
    res.status(200).send({
      status: "success",
      id: createdUser
    });
  } else {
    res.status(400).send({
      status: "error",
      message: "Invalid parameters"
    });
  }
}

exports.updateUser = (req, res, next) => {
  const userReq = req.body;
  const id = req.params.id;
  console.log("Update user request");
  if (checkDataValidity(userReq)) {
    const resp = userModel.updateUser(id, userReq);
    if (!resp) {
      console.log("Update user request > FAIL");
      res.status(500).send({
        status: "error",
        message: "Internal problems"
      });
      return;
    }
    console.log("Update user request > SUCCESS");
    res.status(200).send({
      status: "success",
      message: "Update succesfull",
      id: resp.id
    });
  } else {
    console.log("Update user request > FAIL");
    res.status(400).send({
      status: "error",
      message: "Invalid parameters"
    });
  }
}

// Helpers
function checkDataValidity(data) {
  if (data.familyName) {
    if (data.familyName.trim() === "") {
      return false;
    }
  }
  if (data.givenName) {
    if (data.givenName.trim() === "") {
      return false;
    }
  }
  if (data.email) {
    if (data.email.trim() === "") {
      return false;
    }

    if (!validateEmail(data.email)) {
      return false;
    }
  }
  return true;
}

function validateEmail(email) {
  var rege = /\S+@\S+\.\S+/;
  return rege.test(email);
}
