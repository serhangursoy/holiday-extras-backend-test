// Import the dependencies for testing
const chai = require('chai');
const expect = require('chai').expect
const assert = require('chai').assert
const chaiHttp = require('chai-http');
const app = require('../../app');
const URI = "/users";

// Configure chai
chai.use(chaiHttp);
chai.should();

var FirstPassedUserID = 0;
describe("Users", () => {
  describe("POST /", () => {
    // Test to get all students record
    it("should create an user", (done) => {
      let userObject = {
        givenName: "AutomationGivenName",
        familyName: "AutomationFamilyName",
        email: "AutomationEmail@email.com"
      }
      chai.request(app).post(URI + '/create').send(userObject).end((err, res) => {
        res.should.have.status(200);
        FirstPassedUserID = res.body.id;
        done();
      });
    });
  });
  describe("GET /", () => {
    // Test to get all students record
    it("should get main page", (done) => {
      chai.request(app).get('/').end((err, res) => {
        res.should.have.status(200);
        done();
      });
    });

    it("should get all users", (done) => {
      chai.request(app).get(URI).end((err, res) => {
        res.should.have.status(200);
        res.body.should.be.a('object');
        done();
      });
    });
    // Test to get single student record
    it("should get a single user information", (done) => {
      const id = 1;
      chai.request(app)
        .get(URI + "/" + FirstPassedUserID)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          assert.equal('AutomationGivenName', res.body.user.givenName);
          done();
        });
    });

    it("should not get a single user information", (done) => {

      chai.request(app)
        .get(URI + "/" + FirstPassedUserID + "test")
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          expect(res.body).to.eql({});
          done();
        });
    });

    it("should update the user information", (done) => {
      let userObject = {
        givenName: "AutomationGivenNameUpdated",
        familyName: "AutomationFamilyNameUpdated",
        email: "AutomationEmail@email.com"
      }
      chai.request(app)
        .put(URI + "/" + FirstPassedUserID)
        .send(userObject).end((err, res) => {
          res.should.have.status(200);
          done();
        });
    });

    it("should get a updated user information", (done) => {
      chai.request(app)
        .get(URI + "/" + FirstPassedUserID)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          assert.equal('AutomationGivenNameUpdated', res.body.user.givenName);
          done();
        });
    });

    it("should delete the user", (done) => {
      chai.request(app)
        .delete(URI + "/delete/" + FirstPassedUserID)
        .end((err, res) => {
          res.should.have.status(200);
          done();
        });
    });

  });
});
